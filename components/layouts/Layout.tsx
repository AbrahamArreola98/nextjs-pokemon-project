import React, { FC, ReactNode } from "react";
import Head from "next/head";
import { Link, Navbar, Text, useTheme } from "@nextui-org/react";
import Image from "next/image";
import NextLink from "next/link";

interface Props {
    children: ReactNode;
    title?: string;
}

const origin = typeof window !== "undefined" ? window.location.origin : "";

export const Layout: FC<Props> = ({ children, title }) => {
    const { theme } = useTheme();

    return (
        <>
            <Head>
                <title>{title || "Pokemon App"}</title>
                <meta name="author" content="Abraham Arreola" />
                <meta name="description" content={`Information about ${title}`} />
                <meta name="keywords" content={`${title}, pokemon, pokedex`} />

                <meta property="og:title" content={`${title} information`} />
                <meta property="og:description" content={`Information about ${title}`} />
                <meta property="og:image" content={`${origin}/img/banner.png`} />
            </Head>
            <Navbar
                isCompact
                variant="sticky"
                css={{ backgroundColor: theme?.colors.gray900.value }}
                maxWidth="fluid"
            >
                <Navbar.Brand>
                    <Image
                        src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png"
                        alt="App Icon"
                        width={70}
                        height={70}
                    />
                    <NextLink href="/" passHref>
                        <Link>
                            <Text color="white" h2>
                                P
                            </Text>
                            <Text color="white" h3>
                                ókemon
                            </Text>
                        </Link>
                    </NextLink>
                </Navbar.Brand>
                <Navbar.Content enableCursorHighlight>
                    <NextLink href="/favorites" passHref>
                        <Navbar.Link>Favorites</Navbar.Link>
                    </NextLink>
                </Navbar.Content>
            </Navbar>
            <main style={{ boxSizing: "border-box", padding: 12 }}>{children}</main>
        </>
    );
};
