import React, { FC } from "react";
import { Grid } from "@nextui-org/react";
import { FavoritePokemonCard } from "./";

interface Props {
    pokemonIds: number[];
}

export const FavoritePokemons: FC<Props> = ({ pokemonIds }) => {
    return (
        <Grid.Container gap={2} direction="row" justify="flex-start">
            {pokemonIds.map((pokeId) => (
                <FavoritePokemonCard key={pokeId} pokemonId={pokeId} />
            ))}
        </Grid.Container>
    );
};
