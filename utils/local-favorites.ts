const toggleFavorite = (id: number) => {
    let favorites: number[] = JSON.parse(localStorage.getItem("favorites") || "[]");

    if (favorites.includes(id)) {
        favorites = favorites.filter((pokeId) => pokeId !== id);
    } else {
        favorites.push(id);
    }

    localStorage.setItem("favorites", JSON.stringify(favorites));
};

const isInFavorites = (id: number) => {
    const favorites: number[] = JSON.parse(localStorage.getItem("favorites") || "[]");

    return favorites.includes(id);
};

const favoritePokemons = (): number[] => {
    return JSON.parse(localStorage.getItem("favorites") || "[]");
};

const functions = { toggleFavorite, isInFavorites, favoritePokemons };

export default functions;
